package br.com.cinq.prova.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.cinq.prova.domain.Pois;

@Repository
public interface PoisRepository extends JpaRepository<Pois, Integer>{

}
