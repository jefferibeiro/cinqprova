package br.com.cinq.prova;

import java.text.ParseException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaApplication {

	public static void main(String[] args) throws ParseException {
		SpringApplication.run(ProvaApplication.class, args);
		System.out.println("Inicialização Cinq");
	}

}
