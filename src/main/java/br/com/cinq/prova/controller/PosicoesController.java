package br.com.cinq.prova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import br.com.cinq.prova.domain.Posicoes;
import br.com.cinq.prova.services.PosicoesServices;

@Controller
@RequestMapping(value = "/posicoes")
public class PosicoesController {

	@Autowired
	PosicoesServices posicoesServices;
	
	@GetMapping(value = "/teste")
	public ResponseEntity<?> teste(){
		return ResponseEntity.ok().body("ok");
	}
	
	@GetMapping(value = "/listartodos")
	public ResponseEntity<?> buscar(){
		List<Posicoes> posicoes = posicoesServices.buscar();
		return ResponseEntity.ok().body(posicoes);
	}
	
}
