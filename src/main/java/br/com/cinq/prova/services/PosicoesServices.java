package br.com.cinq.prova.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cinq.prova.domain.Posicoes;
import br.com.cinq.prova.repositories.PosicoesRepository;

@Service
public class PosicoesServices {
	
	@Autowired
	PosicoesRepository posicoesRepository;
	
	public List<Posicoes> buscar(){
		return posicoesRepository.findAll();
	}
}
