package br.com.cinq.prova.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cinq.prova.domain.Pois;
import br.com.cinq.prova.repositories.PoisRepository;

@Service
public class PoisServices {
	
	@Autowired
	PoisRepository poisRepository;

	public List<Pois> buscar(){
		return poisRepository.findAll();
	}
}
