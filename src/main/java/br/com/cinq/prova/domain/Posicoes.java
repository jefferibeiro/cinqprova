package br.com.cinq.prova.domain;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "posicoes")
public class Posicoes {
	
	@Id
	private Integer id;
	private String placa;
	@Column(name = "data_posicao")
	private String dataposicao;
	private Integer velocidade;
	private Double latitude;
	private Double longitude;
	private String ignicao;
	
	public Posicoes() {
		super();
	}

	public Posicoes(Integer id, String placa, String dataposicao, Integer velocidade, Double latitude, Double longitude,
			String ignicao) {
		super();
		this.id = id;
		this.placa = placa;
		this.dataposicao = dataposicao;
		this.velocidade = velocidade;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ignicao = ignicao;
	}
	
	public String getdataFormatada() throws ParseException  {
		String dataErrada = dataposicao.substring(0, 24);
		SimpleDateFormat formatter=new SimpleDateFormat("E MMM dd yyyy", Locale.US); 
		SimpleDateFormat formatter2=new SimpleDateFormat("dd/MM/yyyy");  
		Date date = formatter.parse(dataErrada);
		String date2 = formatter2.format(date); 
		return date2;
	}
	
	public String getHoraFormatada() throws ParseException  {
		String hora = dataposicao.substring(16, 24);
		return hora;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDataposicao() {
		return dataposicao;
	}

	public void setDataposicao(String dataposicao) {
		this.dataposicao = dataposicao;
	}

	public Integer getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Integer velocidade) {
		this.velocidade = velocidade;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getIgnicao() {
		return ignicao;
	}

	public void setIgnicao(String ignicao) {
		this.ignicao = ignicao;
	}

	public String getIgnicaoStatus() {
		if(ignicao.equalsIgnoreCase("true")) {
			return "Ligado";
		}else {
			return "Desligado";
		}
	}
	
	public String getPosicao() {
		return latitude + ", " + longitude;
	}

	@Override
	public String toString() {
		return "Posicoes [id=" + id + ", placa=" + placa + ", dataposicao=" + dataposicao + ", velocidade=" + velocidade
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", ignicao=" + ignicao + "]";
	}
}
