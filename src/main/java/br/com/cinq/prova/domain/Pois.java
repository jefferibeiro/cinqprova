package br.com.cinq.prova.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "base_pois_def")
public class Pois {
	
	@Id
	private String nome;
	
	private Integer raio;
	
	private Double latitude;
	
	private Double longitude;
	
	public Pois() {
		super();
	}

	public Pois(String nome, Integer raio, Double latitude, Double longitude) {
		super();
		this.nome = nome;
		this.raio = raio;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getRaio() {
		return raio;
	}

	public void setRaio(Integer raio) {
		this.raio = raio;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "pois [nome=" + nome + ", raio=" + raio + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}	
}
